## IA02 Projet P24 : Gopher et Dodo

![](figures/mcts_gopher2_gif.gif)

# Sujet 

Ce projet est réalisé dans le cadre de l'UV IA02 au semestre P24. Il vise à implémenter une intelligence artificielle pour jouer aux jeux Gopher et Dodo de Mark Steere.

# Membres

Louis RUBLE (GI04)  
Xavier LEMERLE (GI04)

# Lancement du projet

Prérequis pour lancer le projet : imageio (pour la visualisation de la grille)

```console
pip install imageio
``` 

Lancement de Dodo : 

```console
cd Dodo
python3 main.py
```

Lancement de Gopher : 
```console
cd Gopher
python3 main.py
```

# Méthodes utilisées

Dodo : Minmax  
Gopher : Monte Carlo

D'avantages de détails son rapportés dans les README respectifs des jeux.   

# Résultats 

Les résultats et analyses des algorithmes sont disponibles dans les README du dossier Strategy des deux jeux. 


