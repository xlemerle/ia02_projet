# -*- coding: utf-8 -*-
"""
Created on Tue May 21 19:48:41 2024

@author: lemer
"""
import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))

from typing import Union, Callable
import random
from generals_fonctions import grid_tuple_to_grid_list
from generals_fonctions import grid_list_to_grid_tuple
from generals_fonctions import neighbours
from generals_fonctions import cell_value
from generals_fonctions import get_size
from generals_fonctions import pprint_terminal
from symetric import vertical_symmetry, inverse_1_2
from math import inf

import time

# Types de base utilisés par l'arbitre
Environment = ...  # Ensemble des données utiles (cache, état de jeu...) pour
# que votre IA puisse jouer (objet, dictionnaire, autre...)
Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]  # case de départ -> case d'arrivée
Action = Union[ActionGopher, ActionDodo]
Player = int  # 1 ou 2
State = list[tuple[Cell, Player]]  # État du jeu pour la boucle de jeu
Score = int
Time = int

Strategy = Callable[[State, Player], ActionDodo]


def legals_cell(grid: State, player: Player, cell: Cell) -> list[ActionDodo]:
    '''Renvoie les actions légals pour une celulle d'un joueur'''

    # verification que la valeur de la celulle est bien celle du joueur
    c_value = cell_value(grid, cell)
    if c_value == player:

        size = int(len(grid)**0.5 // 2)
        neighbours_cells = neighbours(cell, size)

        legals_list = []

        for n in neighbours_cells:
            n_value = cell_value(grid, n)
            # verifier que le voisins est libre
            if n_value == 0:
                # si joueur 1 alors cellule en haut
                if player == 1 and (n[0] >= cell[0] and n[1] >= cell[1]):
                    legals_list.append((cell, n))
                # si joueur 2 alors cellule en bas
                elif player == 2 and (n[0] <= cell[0] and n[1] <= cell[1]):
                    legals_list.append((cell, n))

        return legals_list

    else:
        return []


def legals_player(grid: State, player: Player) -> list[ActionDodo]:
    '''Renvoie les actions légales d'un joueur'''

    legals_player_actions = []

    # parcours de toutes les cellules de la grille
    for c in grid:
        cell = c[0]
        if cell and c[1] == player:
            legals_cell_actions = legals_cell(grid, player, cell)
            legals_player_actions.extend(legals_cell_actions)

    return legals_player_actions


def final(grid: State) -> bool:
    '''Renvoie si le jeu est termine'''

    # boucle sur les joueurs
    for player in range(1, 3):

        # renvoie True si aucune actions disponible pour le joueur
        legals_player_actions = legals_player(grid, player)
        if len(legals_player_actions) == 0:
            return True

    # renvoie False sinon
    return False

def target_position_player(grid: State, player: int) -> State:
    '''Renvoie les positions à viser pour une joueur '''

    target_position = []

    for row in grid:
        sum_grid_position = row[0][0] + row[0][1]
        if player == 1 and sum_grid_position >= 2:
            target_position.append(row[0])
        elif player == 2 and sum_grid_position <= -2:
            target_position.append(row[0])

    return grid_list_to_grid_tuple(target_position)

def min_distance(cell: Cell, coordinates: list[Cell]) -> int:
    '''Calcul la distance minimum d'une cellule avec une liste de coordonne'''

    min_distance = inf

    for coordinate in coordinates:
        distance = (cell[0] - coordinate[0])**2 + (cell[1] - coordinate[1])**2
        if distance < min_distance:
            min_distance = distance

    return distance


def evaluation(grid: State) -> float:
    '''Renvoie une evaluation du jeu
    
    Critères pris en compte : 
    1 : nombre de cellule bloque dans les position cibles
    2 : nombre de cellule bloque
    3 : distance plus courte de la cellule à une cellule bloqué dans les positions cibles
    4 : distance avec cases au fond'''

    # position cible = position que doit viser le joueur pour gagner
    targets_positions_p1 = target_position_player(grid, 1)
    targets_positions_p2 = target_position_player(grid, 2)

    # cellule qui sont des positions cibles et bloqués 
    targets_positions_p1_lock = []
    targets_positions_p2_lock = []

    # nombre de positions cibles non bloqués
    nb_targets_positions_not_lock_p1 = 0
    nb_targets_positions_not_lock_p2 = 0

    # nombre de cellule bloqué qui ne sont pas des positions cibles
    nb_not_targets_positions_p1_lock = 0
    nb_not_targets_positions_p2_lock = 0

    # 1ere iteration : pour trouver les cellules (bloque ou non) dans positions cibles
    for row in grid:
        # player 1
        if row[1] == 1:
            if row[0] in targets_positions_p1:
                if legals_cell(grid, 1, row[0]) == []:
                    targets_positions_p1_lock.append(row[0])
                else:
                    nb_targets_positions_not_lock_p1 += 1
            else:
                if legals_cell(grid, 1, row[0]) == []:
                    nb_not_targets_positions_p1_lock += 1
                    

        # player 2
        elif row[1] == 2:
            if row[0] in targets_positions_p2:
                if legals_cell(grid, 2, row[0]) == []:
                    targets_positions_p2_lock.append(row[0])
                else:
                    nb_targets_positions_not_lock_p2 += 1
            else:
                if legals_cell(grid, 2, row[0]) == []:
                    nb_not_targets_positions_p2_lock += 1

    # distance total des cellules non bloqués
    distance_p1 = 0
    distance_p2 = 0

    # cellule depuis laquelle est calculé la distance s'il n'y a aucune cellule bloqué 
    # dans les positions cibles
    grid_size = get_size(grid)
    cell_target_p1 = (-grid_size, -grid_size)
    cell_target_p2 = (grid_size, grid_size)

    # 2eme iteration : pour trouver les distances si aucune cellule dans positions cibles
    for row in grid:
        # player 1
        if row[1] == 1:
            if row[0] not in targets_positions_p1:
                if len(targets_positions_p1_lock) != 0:
                    distance_p1 += min_distance(row[0], targets_positions_p1_lock)
                else:
                    distance_p1 += (row[0][0] - cell_target_p1[0])**2 + (row[0][1] - cell_target_p1[1])**2
        
        # player 2
        elif row[1] == 2:
            if row[0] not in targets_positions_p2:
                if len(targets_positions_p2_lock) != 0:
                    distance_p2 += min_distance(row[0], targets_positions_p2_lock)
                else:
                    distance_p2 += (row[0][0] - cell_target_p2[0])**2 + (row[0][1] - cell_target_p2[1])**2


    
    # calcul du ratio des positions bloqués qui sont des positions cibles 
    if len(targets_positions_p1_lock) != 0 or len(targets_positions_p1_lock) != 0:
        ratio_tp_lock_p1 = len(targets_positions_p1_lock) / (len(targets_positions_p1_lock)+len(targets_positions_p2_lock))
        ratio_tp_lock_p2 = 1 - ratio_tp_lock_p1
    else:
        ratio_tp_lock_p1 = 0
        ratio_tp_lock_p2 = 0
    
    # calcul du ratio des positions bloqués qui ne sont pas des positions cibles
    if nb_not_targets_positions_p1_lock != 0 or nb_not_targets_positions_p2_lock != 0:
        ratio_lock_position_p1 = nb_not_targets_positions_p1_lock / (nb_not_targets_positions_p1_lock + nb_not_targets_positions_p2_lock)
        ratio_lock_position_p2 = 1 - ratio_lock_position_p1
    else:
        ratio_lock_position_p1 = 0
        ratio_lock_position_p2 = 0

    # calcul ratio distance -> objectif pour le joueur : avoir le moins de distance possible
    if distance_p1 != 0 or distance_p2 != 0:
        ratio_distance_p1 = distance_p2 / (distance_p1 + distance_p2)
        ratio_distance_p2 = 1 - ratio_distance_p1
    else:
        ratio_distance_p1 = 0
        ratio_distance_p2 = 0

    # calcul du ratio des positions cibles qui ne sont pas bloqués
    if nb_targets_positions_not_lock_p1 != 0 or nb_targets_positions_not_lock_p2 != 0:
        ratio_tp_not_lock_p1 = nb_targets_positions_not_lock_p1 / (nb_targets_positions_not_lock_p1 + nb_targets_positions_not_lock_p2)
        ratio_tp_not_lock_p2 = 1 - ratio_tp_not_lock_p1
    else:
        ratio_tp_not_lock_p1 = 0
        ratio_tp_not_lock_p2 = 0

    # attribution des poids
    weight_tp_lock = 0.1
    weight_not_tp_lock = 0.2
    weight_distance = 0.7
    weight_tp_not_lock = 0

    # calcul du ratio total (entre 0 et 1)
    ratio_p1 = weight_tp_lock*ratio_tp_lock_p1 + weight_not_tp_lock*ratio_lock_position_p1
    ratio_p1 += weight_distance*ratio_distance_p1 + weight_tp_not_lock*ratio_tp_not_lock_p1

    ratio_p2 = weight_tp_lock*ratio_tp_lock_p2 + weight_not_tp_lock*ratio_lock_position_p2
    ratio_p2 += weight_distance*ratio_distance_p2 + weight_tp_not_lock*ratio_tp_not_lock_p2

    if ratio_p1 >= ratio_p2:
        return ratio_p1 - ratio_p2
    else:
        return -(ratio_p2 - ratio_p1)


def score(grid: State) -> float:
    '''Renvoie le score du jeu'''

    # si etat final
    # renvoie 1 si joueur 1 gagne
    # renvoie 2 si joueur 2 gagne
    for player in range(1, 3):
        legals_player_actions = legals_player(grid, player)
        if len(legals_player_actions) == 0 and player == 1:
            return 1
        elif len(legals_player_actions) == 0 and player == 2:
            return -1

    # si le jeu n'est pas finis, on renvoie l'evaluation du score
    return evaluation(grid)


def play(grid: State, player: Player, action: ActionDodo) -> State:
    '''Joue l'action d'un joueur sur une grille'''

    # conversion de la grille en liste
    grid_list = grid_tuple_to_grid_list(grid)

    # position
    position = action[0]
    new_position = action[1]

    # booleen pour verifier que les cellules sont bien voisines
    neighbours_bool = False
    # booléen pour verifier que les positions indiqués sont bien valable
    position_bool = False
    new_position_bool = False

    position_neighbours = legals_cell(grid, player, position)
    if action in position_neighbours:
        neighbours_bool = True

    for i, cell in enumerate(grid_list):
        # si on trouve la position initial
        if cell[0] == list(position) and cell[1] == player:
            position_index = i
            position_bool = True

        # si on trouve la nouvelle position
        if cell[0] == list(new_position) and cell[1] == 0:
            new_position_index = i
            new_position_bool = True

    # si bool alors application de l'action
    if position_bool and new_position_bool and neighbours_bool:
        grid_list[position_index][1] = 0
        grid_list[new_position_index][1] = player

    else:
        print("Coup non valide.")

    return grid_list_to_grid_tuple(grid_list)



def dodo(grid: State, strategy_1: Strategy, strategy_2: Strategy, debug: bool = False) -> Score:

    start_time = time.time()
    nb_coup = 0
    
    if debug:
        pprint_terminal(grid, get_size(grid))
        print()

    current_player = 1

    while True:
        nb_coup += 1

        if current_player == 1:
            grid_score_action = strategy_1(grid, 1)
        else:
            grid_score_action = strategy_2(grid, 2)
        
        grid_score, action = grid_score_action[0], grid_score_action[1]

        if action not in legals_player(grid, current_player):
            if debug:
                print('Coup invalide', action)
                pprint_terminal(grid, get_size(grid))
                break
            continue

        grid = play(grid, current_player, action)

        if debug:
            print(f'Player {current_player} played at position {action}:')
            eval_grid = evaluation(grid)
            print(f'Evaluation : {eval_grid:.5f}, Score : {grid_score:.5f}')
            pprint_terminal(grid, get_size(grid))
            print()

        # si partie termine
        if final(grid):
            winner = score(grid)
            if debug:
                print('Gagnant : ', winner)
                pprint_terminal(grid, get_size(grid))
                print(f'\nTotal time : {(end_time-start_time):.4f}')
                print('Nombre de coups : ', nb_coup)

            return winner
        
        end_time = time.time()

        # si partie n'est pas termine
        current_player = 2 if current_player == 1 else 1