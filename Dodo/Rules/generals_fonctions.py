# -*- coding: utf-8 -*-
"""
Created on Tue May 21 17:41:59 2024

@author: lemer
"""
from typing import Union


# Types de base utilisés par l'arbitre
Environment = ...  # Ensemble des données utiles (cache, état de jeu...) pour
# que votre IA puisse jouer (objet, dictionnaire, autre...)
Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]  # case de départ -> case d'arrivée
Action = Union[ActionGopher, ActionDodo]
Player = int  # 1 ou 2
State = list[tuple[Cell, Player]]  # État du jeu pour la boucle de jeu
Score = int
Time = int


def grid_tuple_to_grid_list(grid: State) -> list[list[list[int, int], Player]]:
    '''Transforme un State de tuple en list'''

    grid_list = []
    for row in grid:
        row_list = []
        for element in row:
            # si l'élément est un tuple on convertit en liste
            if isinstance(element, tuple):
                row_list.append(list(element))
            # sinon ajout classique
            else:
                row_list.append(element)
        grid_list.append(row_list)

    return grid_list


def grid_list_to_grid_tuple(grid: list[list[list[int, int], Player]]) -> State:
    '''Transforme un State de list en tuple'''
    grid_tuple = []
    for row in grid:
        if isinstance(row[0], list):
            row_tuple = ((tuple(row[0]), row[1]))
        else:
            row_tuple = tuple(row)
        grid_tuple.append(row_tuple)
    return grid_tuple


def pprint_terminal(grid: State, size: int):
    '''affiche la grille dans le terminal'''
    
    grid_size = int(len(grid)**0.5 // 2)
    grid_height = grid_size * 4 + 1  # *4+1 est déterminé de par mes observations

    # initialisation de la grille pour afficher l'hexagone
    pprint_grid = [[] for _ in range(grid_height)]

    # calcul d'une nouvelle grille en fonction de la hauteur de la cellule
    for row in grid:
        # si la ligne existe (coordonnees differente de None)
        if row[0]:
            # indice pour pprint_grid
            index_heigth = 2*grid_size - sum(row[0])
            pprint_grid[index_heigth].append(row[1])

    # affichage du tableau
    for i, row in enumerate(pprint_grid):
        decalage = grid_size + 1 - len(row)
        print(' '*decalage, end='')
        for j, val in enumerate(row):
            print(val, end=' ')
        print()


def neighbours(cell: Cell, size: int) -> list[Cell]:
    '''Fonction qui renvoie tous les voisins d'une cellule'''

    # extraction des coordonnes
    i, j = cell[0], cell[1]

    # extraction des coordonnees voisins
    # coordonnees horaires
    h0 = (i+1, j+1)
    h2 = (i+1, j)
    h4 = (i, j-1)
    h6 = (i-1, j-1)
    h8 = (i-1, j)
    h10 = (i, j+1)

    all_neighbours = [h0, h2, h4, h6, h8, h10]
    valid_neighbours = []

    for n in all_neighbours:
        if abs(n[0]) <= size and abs(n[1]) <= size:
            valid_neighbours.append(n)

    return valid_neighbours


def cell_value(grid: State, cell: Cell) -> int:
    '''Renvoie la valeur (0, 1 ou 2) d'une cellule dans une grille'''

    for row in grid:
        if row[0] == cell:
            return row[1]
    return None

def get_size(grid: State) -> int:
    '''Renvoie la taille d'une grille'''

    max = 0
    for row in grid:
        if abs(row[0][0]) > max:
            max = row[0][0]
        elif abs(row[0][1]) > max:
            max = row[0][1]

    return max