import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../')))
from generals_fonctions import cell_value, grid_list_to_grid_tuple, grid_tuple_to_grid_list
from typing import Union, Callable

# Types de base utilisés par l'arbitre
Environment = ...  # Ensemble des données utiles (cache, état de jeu...) pour
# que votre IA puisse jouer (objet, dictionnaire, autre...)
Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]  # case de départ -> case d'arrivée
Action = Union[ActionGopher, ActionDodo]
Player = int  # 1 ou 2
State = list[tuple[Cell, Player]]  # État du jeu pour la boucle de jeu
Score = int
Time = int

Strategy = Callable[[State, Player], ActionDodo]

def test_vertical_symmetry(grid1, grid2):
    '''Test si deux grilles sont symetriques verticalement'''

    for row in grid1:
        if row[0]: # si la ligne n'est pas nul 
            i, j = row[0][0], row[0][1]
            cell_grid2 = cell_value(grid2, (j, i))
            if row[1] != cell_grid2:
                return False
            
    return True

def vertical_symmetry(grid):
    '''Renvoie la symetrie verticale de la grille'''

    grid_sym = grid_tuple_to_grid_list(grid)

    for row in grid_sym:
        i, j = row[0][0], row[0][1]
        row[1] = cell_value(grid, (j, i))
        
    return grid_list_to_grid_tuple(grid_sym)

def horizontal_symmetry(grid):
    '''Renvoie la symetrie horizontale de la grille'''

    grid_sym = grid_tuple_to_grid_list(grid)

    for row in grid_sym:
        i, j = row[0][0], row[0][1]
        row[1] = cell_value(grid, (-j, -i))

    return grid_list_to_grid_tuple(grid_sym)

def inverse_1_2(grid):
    '''Renvoie la grille inversé entre les joueurs 1 et 2'''

    grid_inverse = horizontal_symmetry(vertical_symmetry(grid))
    grid_inverse = grid_tuple_to_grid_list(grid_inverse)

    for row in grid_inverse:
        if row[1] == 1:
            row[1] = 2
        elif row[1] == 2:
            row[1] = 1

    return grid_list_to_grid_tuple(grid_inverse)