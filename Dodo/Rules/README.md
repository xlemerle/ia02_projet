# Contenu des fichiers .py

Dossier contenant les fichier permettant de jouer au jeu Dodo.

**dodo.py**

Fichier contenant les règles principales du jeu Dodo : 

```python
def legals_cell(grid: State, player: Player, cell: Cell) -> list[ActionDodo]
    '''Renvoie les actions légals pour une celulle d'un joueur'''

def legals_player(grid: State, player: Player) -> list[ActionDodo]:
    '''Renvoie les actions légales d'un joueur'''

def final(grid: State) -> bool:
    '''Renvoie si le jeu est termine'''
```

Dans ce fichier se trouve également la fonction d'évaluation du jeu ainsi que des fonctions annexes permettant de calculer l'évaluation du jeu. 

```python
def evaluation(grid: State) -> float:
    '''Renvoie une evaluation du jeu'''
```

L'évaluation du jeu Dodo dépend de 4 critères : 
1. Nombre de cellule bloque dans les position cibles
2. Nombre de cellule bloque
3. Distance plus courte de la cellule à une cellule bloqué dans les positions cibles
4. Distance avec cases au fond

Ces 4 critères sont pondérés avec des poids. Le poids d'un critère varie entre 0 et 1, et la somme des poids des 4 critères doit être égale à 1. Afin de savoir quelle est la meilleur combinaison des poids, tous les poids sont testés par interval de 0.1. En respectant la contrainte de la somme des poids égale à 1, cela représente au total 256 combinaisons. 

Ensuite, ne sont gardés que les combinaisons qui jouant contre elle-même avec l'algorithme minmax en commençant remporte la partie. Puis sont gardés les 30 meilleurs combinaisons, c'est-à-dire celle renvoyant la meilleur évaluation finale. 

Ces 30 combinaisons jouent par la suite 50 parties contre une stratégie random. Si la statégie minmax gagne la partie, le score est de 1, sinon de -1. Au final, pour chacune des 30 combinaisons, on obtient un score entre -1 et 1. La combinaison avec le meilleur score est retenue. 

Les résultats sont disponibles dans le fichier ```weights_combinations_result.csv```

**generals_fonctions.py**

Ce fichier contient des fonctions générales concernant les grilles hexagonales, telles que les fonctions pour transformer la grille d'un tuple vers une liste et inversement. Les autres fonctions renvoient les voisins d'une cellule, la valeur d'une cellule selon des coordonnées, la taille de la grille, et afficher la grille de façon simpliste sur le terminal. 

**symetric.py**

Les fonctions servant à calculer les symétries de grille hexagonales pour Dodo se situent dans ce fichier. 

**utils_plotting.py**

Ce fichier est utilisé pour afficher la grille de manière esthétique.