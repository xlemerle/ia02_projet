# Rules

Le dossier ```Rules``` contient les fichiers python avec les règles nécessaires pour jouer à Dodo ainsi que la fonction d'évaluation. 

# Strategy 

Le dossier ```Strategy``` contient le fichier ```dodo_minmax``` contenant la stratégie utilisé pour Dodo. Comme son nom l'indique, la statégie minmax est utilisée. 

# Test en local

Afin de tester la stratégie deployée pour le jeu Dodo, il suffit de lancer le fichier ```main.py```. Une partie est simulée entre une stratégie minmax et une stratégie random. 