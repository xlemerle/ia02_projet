# Description de la stratégie utilisé pour Dodo

La stratégie utilisé pour Dodo est l'algorithme minmax avec un cache. 

## Minmax

L'algorithme minmax a été implémenté en étant adapté pour le jeu Dodo. 

```python
def minmax(grid: State, player: Player, depth: int=0) -> float:
    '''Renvoie le score optimal de la partie par l'algo minmax'''
```

La première fonction, ```minmax```, a été élaboré dans un premier temps. Elle ne renvoit que l'évaluation de la grille. 

```python
def minmax_action(grid: State, player: Player, depth: int=0) -> tuple[float, ActionDodo]:
    '''Renvoie l'action et l'eval du jeu'''
```

La fonction ```minmax_action``` s'est insipiré de la fonction ```minmax```, en renvoyant en plus le coup à jouer. 

## Cache

Un cache a été ajouté pour ```minmax```, puis adapté pour la fonction ```minmax_action```. Une difficulté pour le cache a été de renvoyer l'action à jouer si et seulement si le joueur pour l'état de la grille actuel, le joueur est le même que celui sauvegardé dans le cache. 

En effet, si par exemple pour un état de la grille sauvegardé dans le cache avec une action à jouer pour le joueur 2, alors si le même état de grille est détecté lorsque c'est le joueur 1 qui joue, il faut faire attention que le joueur 1 ne joue pas l'action du joueur 2. 

## Ouverture 

Les 4 premiers coups sont automatique pour le début de la partie. En effet, une ouverture est jouée. Pour cela, la fonction ```strategy_minmax``` détecte si l'état de la grille permet de jouer une ouverture, et la joue dans le cas échéant. Les ouvertures sont disponibles pour les deux joueurs. 

## Fonction annexe

Une fonction ```strategy_random``` est utilisé afin de tester l'algorithme contre un joueur random. 

## Résultats


Les résulats n'ont pas été à la hauteur. En effet, l'algorithe a perdu les trois parties qu'il a disputé. Ces choix peuvent s'expliquer par plusieurs éléments. 

**Fonction d'évaluation :** La fonction d'évaluation n'évalue pas au mieux la situation du jeu. Afin d'améliorer cette fonction d'évaluation, on peut par exemple prendre en compte le nombre de coup possible par joueur. Ce critère sera simple à implémenter dans la fonction d'évaluation actuelle grâce aux poids des autres critères. 

**Gestion du temps :** L'algorithme minmax était configuré pour une profondeur de 4. Cependant, les parties étaient très courtes et l'algorithme aurait pu augmenter la profondeur d'un niveau. 

**Gestion du cache :** Un cache était mis en place, toutefois ce dernier était vidé après le coup joué. Ce choix était justifié car en gardant le cache coup après coup, la fonction évalue des états de jeu à des profondeur faible, et donc peu fiable. Garder le cache toute la partie pour un gain de temps (afin d'augmenter encore la profondeur) aurait pu être bénéfique.  
