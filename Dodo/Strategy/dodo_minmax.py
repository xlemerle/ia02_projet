import time
import random
from math import inf
from typing import Union, Callable
import numpy as np
import sys
import os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../Rules')))

from generals_fonctions import get_size
from dodo import legals_player, play, score, evaluation, final, dodo
from symetric import vertical_symmetry, inverse_1_2



# Types de base utilisés par l'arbitre
Environment = ...  # Ensemble des données utiles (cache, état de jeu...) pour
# que votre IA puisse jouer (objet, dictionnaire, autre...)
Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]  # case de départ -> case d'arrivée
Action = Union[ActionGopher, ActionDodo]
Player = int  # 1 ou 2
State = list[tuple[Cell, Player]]  # État du jeu pour la boucle de jeu
Score = int
Time = int

Strategy = Callable[[State, Player], ActionDodo]



def strategy_random(grid: State, player: Player) -> tuple[float, ActionDodo]:
    '''Strategie aleatoire'''

    legals_player_actions = legals_player(grid, player)
    random_index = random.randint(0, len(legals_player_actions)-1)
    return (evaluation(grid), legals_player_actions[random_index])

def memoize_minmax(f: Callable[[State, Player, int], float]) -> Callable[[State, Player, int], float]:
    '''Cache de la fonction minmax'''
    cache = {}

    def g(grid, player, depth):

        hashable_grid = tuple(tuple(x) for x in grid)

        vert_sym_grid = vertical_symmetry(grid)
        hashable_vert_sym_grid = tuple(tuple(x) for x in vert_sym_grid)

        inverse_grid = inverse_1_2(grid)
        hashable_inverse_grid = tuple(tuple(x) for x in inverse_grid)
        
        
        if hashable_grid in cache:
            return cache[hashable_grid]
        elif hashable_vert_sym_grid in cache:
            return cache[hashable_vert_sym_grid]
        elif hashable_inverse_grid in cache:
            return -cache[hashable_inverse_grid]    
    
        val = f(grid, player, depth)
        cache[hashable_grid] = val
        return val
    
    return g

@memoize_minmax
def minmax(grid: State, player: Player, depth: int=0) -> float:
    '''Renvoie le score optimal de la partie par l'algo minmax'''

    actions = legals_player(grid, player)

    # 1ere Condition d'arret : partie terminée
    if final(grid):
        return (score(grid))

    # 2eme condition d'arret : profondeur atteinte
    elif depth == 0:
        return (evaluation(grid))

    actions = legals_player(grid, player)

    # max player (1)
    if player == 1:
        bestValue = -inf
        for action in actions:
            grid_action = play(grid, player, action)
            v = minmax(grid_action, 2, depth-1)
            bestValue = max(bestValue, v)
            #print(player, bestValue)
        return bestValue

    elif player == 2:
        bestValue = inf
        for action in actions:
            grid_action = play(grid, player, action)
            v = minmax(grid_action, 1, depth-1)
            bestValue = min(bestValue, v)
            #print(player, bestValue)
        return bestValue
    
def memoize_minmax_action(f: Callable[[State, Player, int], tuple[float, ActionDodo]]) -> Callable[[State, Player, int], tuple[float, ActionDodo]]:
    '''Cache de la fonction minmax_action'''

    cache = {}

    def g(grid, player, depth):

        hashable_grid = tuple(tuple(x) for x in grid)       

        #vert_sym_grid = vertical_symmetry(grid)
        #hashable_vert_sym_grid = tuple(tuple(x) for x in vert_sym_grid)

        #inverse_grid = inverse_1_2(grid)
        #hashable_inverse_grid = tuple(tuple(x) for x in inverse_grid)
        
        
        if hashable_grid in cache and player == cache[hashable_grid][2]:
            return (cache[hashable_grid][0], cache[hashable_grid][1])
        #elif hashable_vert_sym_grid in cache:
        #    return cache[hashable_vert_sym_grid]
        #elif hashable_inverse_grid in cache:
        #    return -cache[hashable_inverse_grid]
        
        (val, action) = f(grid, player, depth)
        cache[hashable_grid] = (val, action, player)
        return (val, action)
    
    g.cache = cache
    g.clear_cache = lambda: cache.clear()

    return g

@memoize_minmax_action    
def minmax_action(grid: State, player: Player, depth: int=0) -> tuple[float, ActionDodo]:
    '''Renvoie l'action et l'eval du jeu'''

    actions = legals_player(grid, player)

    # 1ere Condition d'arret : partie terminée
    if final(grid):
        return (score(grid), None)

    # 2eme condition d'arret : profondeur atteinte
    elif depth == 0:
        return (evaluation(grid), actions[0])
    
    elif player == 1:
        bestValue = -inf
        bestAction = None
        for action in actions:
            grid_action = play(grid, player, action)
            value_action = minmax_action(grid_action, 2, depth -1)
            value = value_action[0]
            bestValue = max(bestValue, value)
            if bestValue == value:
                bestAction = action
        return (bestValue, bestAction)
    
    elif player == 2:
        bestValue = inf
        bestAction = None
        for action in actions:
            grid_action = play(grid, player, action)
            value_action = minmax_action(grid_action, 1, depth -1)
            value = value_action[0]
            bestValue = min(bestValue, value)
            if bestValue == value:
                bestAction = action
        return (bestValue, bestAction)
    
def detect_opening(grid: State, player: int):
    '''Detect si le joueur doit jouer une ouverture'''
    # position initial 

    opening1 = True
    opening2 = True
    opening3 = True
    opening4 = True

    for row in grid:
        sum_coord =  row[0][0] + row[0][1]
        if player == 1:
            # ouverture pour coup 1
            if sum_coord <= -2 and not row[1] == 1:
                # condition pour opening 1
                opening1 = False
                if row[0] != (-1, -1):
                    opening2 = False
                if row[0] != (-1, -2):
                    opening3 = False
                if row[0] != (-2, -2):
                    opening4 = False

            if row[0] == (-1, 0) and row[1] != 1:
                opening2, opening3, opening4 = False, False, False

        elif player == 2:
            if sum_coord >= 2 and not row[1] == 2:
                opening1 = False
                if row[0] != (1, 1):
                    opening2 = False
                if row[0] != (1, 2):
                    opening3 = False
                if row[0] != (2, 2):
                    opening4 = False

            if row[0] == (1, 0) and row[1] != 2:
                opening2, opening3, opening4 = False, False, False

    return [opening1, opening2, opening3, opening4]
    
def strategy_minmax(grid: State, player: Player, time_left: Time = 100) -> tuple[float, ActionDodo]:
    '''strategy minmax'''

    # inversion de 1 et 2 car j'ai confondu 

    minmax_action.clear_cache()

    grid_size = get_size(grid)
    #print('Grid size : ', grid_size)

    opening1_p1 = (((-1, -1), (-1, 0)), ((-1, -2), (-1, -1)), 
            ((-2, -2), (-1, -2)), ((-3, -3), (-2, -2)))

    opening1_p2 = (((1, 1), (1, 0)), ((1, 2), (1, 1)), 
               ((2, 2), (1, 2)), ((3, 3), (2, 2))) 

    if grid_size == 2:
        return minmax_action(grid, player, depth=5)
    
    elif grid_size == 3:
        # verification si on peut jouer une ouverture
        opening_possibility = detect_opening(grid, player) 
        for i in range(len(opening_possibility)):
            if opening_possibility[i]:
                if player == 1:
                    return (evaluation(grid), opening1_p1[i])
                elif player == 2:
                    return (evaluation(grid), opening1_p2[i])
        
        if time_left <= 5:
            return minmax_action(grid, player, depth=2)  
        else:
            return minmax_action(grid, player, depth=3)      


        '''elif time_left <= 75:
            return minmax_action(grid, player, depth=3)      
        else:
            return minmax_action(grid, player, depth=4)'''  

    elif grid_size == 4:     
        if time_left <= 100:
            return minmax_action(grid, player, depth=2)
        else:
            return minmax_action(grid, player, depth=3)

    
    else:
        return minmax_action(grid, player, depth=2)

