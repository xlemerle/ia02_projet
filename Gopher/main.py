from Rules.rules import init_grid, gopher
from Strategies.strategies import strategy_random, strategy_MCTS


def main():
    # Paramètres de la partie
    grid_size = 5
    debug = True
    gif = False

    # Initialisation de la grille
    grid = init_grid(grid_size)

    # Choix des stratégies
    strategy_player1 = strategy_MCTS
    strategy_player2 = strategy_random

    # Lancement de la partie
    print("Début de la partie")
    result = gopher(grid, strategy_player1, strategy_player2, grid_size, debug, gif)

    # Affichage du résultat
    if result > 0:
        print("Le joueur 1 a gagné!")
    elif result < 0:
        print("Le joueur 2 a gagné!")
    else:
        print("Match nul!")


if __name__ == "__main__":
    main()
