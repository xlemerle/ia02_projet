# Implémentation de Monte Carlo Tree Search (MCTS)

Le contribution principale de ce projet pour Gopher est l'implémentation d'une stratégie MCTS from scratch, avec propagation inverse, en utilisant un dictionnaire pour les nœuds explorés et en tenant compte des simulations précédentes.


## Nombre de simulations dynamique et stockage des coups

 Le nombre de simulations à effectuer est calculé de manière dynamique selon la formule 
 
```python
 nb_simulations = 1 + int(80 * calculer_prop_plateau(grid)).
```

 Cela signifie que plus la partie avance (et que le plateau se remplit), plus le nombre de simulations augmente. Ce choix permet d'explorer plus en profondeur lorsque la partie est engagée, tout en économisant du temps de calcul au début. Cette décision a été prise après avoir joué plusieurs parties contre l'IA de Ludii et en constatant qu'il n'était pas nécessaire d'être précis pendant l'ouverture.

L'ensemble des coups joués avec l'issue de la partie résultante sont stockés dans un dictionnaire à l'aide d'un hashage de la position.
L'utilisation de multithreading pour la simulation des parties en parallèle aurait surement permis de gagner en temps de calcul mais n'a malheureusement pas pu être explorée.

## Filtrage des coups non prometteurs

Avant d'effectuer les simulations, l'algorithme filtre les coups légaux pour ne garder que les coups "prometteurs". Un coup est considéré comme non prometteur s'il mène à une position déjà évaluée négativement pour le joueur courant. Ce filtrage réduit le nombre de simulations à effectuer, accélérant ainsi les calculs.

## Réutilisation des évaluations précédentes

L'implémentation stocke les évaluations moyennes (score cumulé et nombre de simulations) de chaque position de jeu déjà explorée dans un dictionnaire evaluations. Lors de l'initialisation des statistiques pour un coup, si la position résultante a déjà été évaluée, les valeurs stockées sont réutilisées. Cela évite de repartir de zéro pour les positions déjà connues.

## Arrêt prématuré des simulations
Les coups légaux sont filtrés en utilisant des évaluations antérieures pour éliminer les coups non pertinents, réduisant ainsi la charge de simulation

Pour chaque coup, les simulations s'arrêtent prématurément si un certain score négatif est atteint. Cela permet d'économiser du temps de calcul lorsqu'un coup s'avère clairement mauvais pour le joueur courant.

## Formule UCT

L'algorithme utilise la formule standard de l'UCT (Upper Confidence Bound) offre un bon équilibre entre l'exploration et l'exploitation des coups. Le coefficient sqrt(2) est utilisé comme paramètre d'exploration.

## Sélection du meilleur coup

Le meilleur coup est sélectionné en choisissant celui avec la valeur UCT maximale pour le joueur 1 (maximisant), ou minimale pour le joueur 2 (minimisant).

## Résultats
À l'issue du tournoi, notre algorithme MCTS a obtenu un score équilibré de 2 victoires pour 2 défaites. L'analyse des parties révèle plusieurs points d'amélioration cruciaux :

**Gestion du temps :** Une position avantageuse s'est soldée par une défaite due à un dépassement du à la pendule. Ceci met en lumière la nécessité d'optimiser le déterminisme de notre algorithme. Une piste d'amélioration serait de développer des versions spécifiques pour différentes contraintes de temps (10 secondes, 20 secondes, 1 minute).  

**Stratégie d'ouverture :** Notre seconde défaite a été causée par une prise d'avantage rapide de l'adversaire, dès les 7-8 premiers coups. Cela remet en question notre hypothèse initiale concernant la phase d'ouverture et nécessite une révision approfondie de cette étape cruciale. 

**Optimisation des simulations :** Le nombre de simulations effectuées devrait suivre une progression logarithmique plutôt que linéaire. Cette approche permettrait une meilleure allocation des ressources , évitant ainsi un gaspillage de temps dans les phases finales tout en assurant une recherche approfondie durant la transition entre l'ouverture et le milieu de jeu.

Ces observations ouvrent plusieurs axes de recherche et développement pour améliorer les performances de notre IA dans les futures compétitions.

## Ressources utilisées
- Cours de IA02
- IA de Gopher Ludii