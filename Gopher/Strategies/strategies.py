import random
import math
from functools import lru_cache
from Rules.rules import (
    legals,
    play,
    other_player,
    final,
    score,
    hash_grid,
    init_grid,
    calculer_prop_plateau,
)
from Rules.rules import State, Player, Action, ActionDodo, Score


def strategy_random(grid: State, player: Player, size: int) -> ActionDodo:
    """
    Strategie aléatoire
    """
    legals_player_actions = legals(grid, player, size)
    random_index = random.randint(0, len(legals_player_actions) - 1)
    return legals_player_actions[random_index]


def strategy_brain(grid: State, player: Player, size: int) -> Action:
    """
    Strategie pour faire jouer humain
    """
    print("à vous de jouer: ", end="")
    s = input()
    print()
    t = ast.literal_eval(s)
    return t


@lru_cache(maxsize=None)
def minmax(grid_tuple: tuple, player: Player, size: int) -> tuple:
    """
    Version de minmax avec cache , immense raté , pas de profondeur, approche complètement naive.
    """
    grid = list(grid_tuple)  # Convertir le tuple en liste pour pouvoir le modifier
    if final(grid, player, size):
        return None, None  # Aucune action à jouer dans un état final

    best_action = None
    shortest_depth = float("inf")

    for action in legals(grid, player, size):
        new_grid = play(grid, player, action, size)
        new_grid_tuple = tuple(new_grid)
        # Convertir la liste en tuple pour la mémoïzation  avec lru cache
        opponent_result = minmax(new_grid_tuple, other_player(player), size)

        if opponent_result[0] is None:
            # L'adversaire n'a pas de coup à jouer, donc on gagne
            return action, 1

        current_depth = opponent_result[1] + 1 if opponent_result[1] is not None else 1
        # Strategie ou on priorise la partie la plus courte
        if best_action is None or current_depth < shortest_depth:
            best_action = action
            shortest_depth = current_depth

    return best_action, shortest_depth


def strategy_minmax(grid: State, player: Player, size: int) -> Action:
    """
    Strategy minmax
    """
    grid_tuple = tuple(grid)  # Convertir la liste en tuple pour la mémoïzation
    best_move = minmax(grid_tuple, player, size)
    return best_move if best_move else strategy_random(grid, player, size)


# Stocker les coups pour la backpropagation
best_moves_dict = {}
evaluations = {}


def strategy_MCTS(grid: State, player: Player, size: int) -> Action:
    """
    Fonction MCTS from scratch avec backpropagation en utilisant un dictionnaire  nœuds explorés, en tenant compte des simulations précédentes.
    Utilise la même logique que la stratégie aléatoire pour simuler les parties.
    """
    if grid == init_grid(size):
        print("ok")
        return (0, 0)

    w_i = []
    n_i = []
    # Plus l'on avance dans la partie moins les coups joués sont aléatoires
    nb_simulations = 1 + int(80 * calculer_prop_plateau(grid))
    coups_legals = legals(grid, player, size)

    # Filtrer les coups non pertinents afin d'avoir le moins de simulations a faire
    if len(coups_legals) > 4:
        for i, legal in enumerate(coups_legals):
            grid_n = play(grid, player, legal, size)
            grid_hash = hash_grid(grid_n)
            if grid_hash in evaluations:
                _, score = evaluations[grid_hash]
                if (score > 0 and player == 2) or (score < 0 and player == 1):
                    coups_legals.pop(i)

    for i, legal in enumerate(coups_legals):
        grid_n = play(grid, player, legal, size)
        grid_hash = hash_grid(grid_n)
        if grid_hash in evaluations:
            n_k, sum_evaluations = evaluations[grid_hash]
            w_i.append(sum_evaluations)
            n_i.append(n_k)
        else:
            w_i.append(0)
            n_i.append(0)
        for j in range(nb_simulations):
            grid_nj = grid_n
            w_i[i] += simulate_random_game(grid_nj, other_player(player), size)
            n_i[i] += 1
            # Si résultats mauvais sur un noeud on stoppe net les simulations
            if (w_i[i] == -3 and player == 1) or (w_i[i] == 3 and player == 2):
                break

    # Calcul de l'UCT (cf cours IA02)
    UCT = [w / n + math.sqrt(2) * n / sum(n_i) for w, n in zip(w_i, n_i)]
    print(nb_simulations, [_ for _ in zip(legals(grid, player, size), UCT, n_i)])
    idx = max(w_i) if player == 1 else min(w_i)
    return legals(grid, player, size)[w_i.index(idx)]


def simulate_random_game(grid: State, player: Player, size: int) -> Score:
    """
    Simule une partie en jouant des coups aléatoires jusqu'à la fin de la partie.
    Renvoie le score final de la partie.
    """
    current_player = player
    while not final(grid, current_player, size):
        legal_moves = legals(grid, current_player, size)
        if not legal_moves:
            break
        random_move = random.choice(legal_moves)
        grid = play(grid, current_player, random_move, size)
        current_player = other_player(current_player)
        grid_hash = hash_grid(grid)

        if grid_hash in evaluations:
            evaluations[grid_hash][0] += 1
            evaluations[grid_hash][1] += score(grid, player, size)
        else:
            evaluations[grid_hash] = [1, score(grid, player, size)]

    return score(grid, player, size)
