Vous retrouverez dans ce dossier l'ensembles des fonctions du jeu de Gopher divisées en 2 parties : 
- **Rules** : différentes règles, affichage du plateau et des jetons, boucle de jeu.
- **Strategies** : algorithmes de prises de décision des coups: random, MinMax, MCTS.