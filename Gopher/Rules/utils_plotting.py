import math
import collections
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
import imageio


# Disposition des grilles : largement inspiré de https://www.redblobgames.com/grids/hexagons/implementation.html
Orientation = collections.namedtuple(
    "Orientation", ["f0", "f1", "f2", "f3", "b0", "b1", "b2", "b3", "start_angle"]
)
Layout = collections.namedtuple("Layout", ["orientation", "size", "origin"])
Point = collections.namedtuple("Point", ["x", "y"])
Hex = collections.namedtuple("Hex", ["q", "r", "s"])

layout_pointy = Orientation(
    math.sqrt(3.0),
    math.sqrt(3.0) / 2.0,
    0.0,
    3.0 / 2.0,
    math.sqrt(3.0) / 3.0,
    -1.0 / 3.0,
    0.0,
    2.0 / 3.0,
    0.5,
)
layout_flat = Orientation(
    3.0 / 2.0,
    0.0,
    math.sqrt(3.0) / 2.0,
    math.sqrt(3.0),
    2.0 / 3.0,
    0.0,
    -1.0 / 3.0,
    math.sqrt(3.0) / 3.0,
    0.0,
)


def find_cell_position(data, cell):
    """
    Trouve la position d'une cellule dans une grille 2D.
    Retourne les indices (ligne, colonne) de la cellule si trouvée, None sinon.
    """
    for row_idx, row in enumerate(data):
        for col_idx, element in enumerate(row):
            if element == cell:
                return row_idx, col_idx
    return None


def grid_play_to_grid_print(grid, size):
    """
    Convention differentes entre red blob games et le projet IA02
    Convertit une grille de jeu en grille d'affichage.
    Ajuste les coordonnées pour l'affichage correct des hexagones.
    """
    output = np.array(grid).copy()
    for i in range(2 * size + 1):
        for j in range(2 * size + 1):
            if i != size:
                if output[i, j] is not None:
                    output[i, j] = (output[i, j][0], -output[i, j][1])
    return output


def convert_cell(grid_play, cell_play, size):
    """
    Convertit les coordonnées d'une cellule de la grille de jeu à la grille d'affichage.
    """
    idx = find_cell_position(grid_play, cell_play)
    if idx == None:
        return (None, None)
    output = grid_play_to_grid_print(grid_play, size)[idx]
    return output


def hex_to_pixel(layout, h):
    """
    Convertit des coordonnées hex en coordonnées pixel
    """
    M = layout.orientation
    size = layout.size
    origin = layout.origin
    x = (M.f0 * h.q + M.f1 * h.r) * size.x
    y = (M.f2 * h.q + M.f3 * h.r) * size.y
    return Point(x + origin.x, y + origin.y)


def pixel_to_hex(layout, p):
    """
    Convertit des coordonnées pixels en coordonnées hex
    Inverse de l'opé  hex_to_pixel.
    """
    M = layout.orientation
    size = layout.size
    origin = layout.origin
    pt = Point((p.x - origin.x) / size.x, (p.y - origin.y) / size.y)
    q = M.b0 * pt.x + M.b1 * pt.y
    r = M.b2 * pt.x + M.b3 * pt.y
    return Hex(q, r, -q - r)


def plot_hex(ax, center, size, color="k", text=""):
    """
    Dessine un hexagone sur l'axe donné.
    Remplit l'hexagone avec la couleur spécifiée et ajoute du texte au centre
    """
    corners = []
    for i in range(6):
        angle = 2.0 * math.pi * (i + layout_pointy.start_angle) / 6
        x_i = center.x + size * math.cos(angle)
        y_i = center.y + size * math.sin(angle)
        corners.append((x_i, y_i))

    corners.append(corners[0])  # Closing the hexagon
    xs, ys = zip(*corners)
    ax.plot(xs, ys, "k-")
    ax.fill(xs, ys, color=color, alpha=0.6)  # Fill the hex with color
    ax.text(center.x, center.y, text, ha="center", va="center", fontsize=8)


size = Point(1, 1)
origin = Point(2, 2)


def plot_hex_grid(layout, radius, colored_hexes, figure=True):
    """
    Dessine une grille hexagonale avec matplotlib
    Colorie les hexagones spécifiés et ajoute des coordonnées en texte.
    Retourne l'image sous forme de tableau NumPy pour potentiel gif.
    """
    hex_grid = [
        [None] * (2 * radius + 1) for _ in range(2 * radius + 1)
    ]  # Création de la grille hexagonale

    fig, ax = plt.subplots()
    ax.set_aspect("equal")

    points = []
    for r in range(radius, -radius - 1, -1):
        q_min = max(-radius, -r - radius)
        q_max = min(radius, -r + radius)
        for q in range(q_min, q_max + 1):
            hex = Hex(q, r, -q - r)
            point = hex_to_pixel(layout, hex)
            points.append(point)
            hex_grid[radius + r][radius + q] = (hex.q, hex.r)
    hex_grid = np.array(hex_grid, dtype=object)
    for i, point in enumerate(points):
        check = False
        hex = pixel_to_hex(layout, point)
        cell = convert_cell(hex_grid, (round(hex.q), round(hex.r)), radius)
        color = "k"  # Default color
        text = f"({cell[0]}, {cell[1]})"
        for ch in colored_hexes:
            if (cell[0], cell[1]) == (ch[0], ch[1]):
                color = ch[2]
                plot_hex(ax, point, layout.size.x, color, text)
                check = True
                break
        if not check:
            plot_hex(ax, point, layout.size.x, color, text)
    ax.set_axis_off()  # Désactiver les axes

    if figure:
        plt.show()

    # Convertir la figure en image NumPy
    fig.canvas.draw()
    img = np.frombuffer(fig.canvas.tostring_rgb(), dtype=np.uint8)
    img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))

    return img


layout = Layout(layout_flat, size, origin)
colored_hexes = [
    (0, 0, "red"),
    (1, -1, "blue"),
    (-1, 1, "blue"),
]


def generate_colored_hexes(grid):
    """
    Génère une liste d'hexagones colorés à partir d'une grille.
    Convertit les codes couleur en noms de couleurs ('red' ou 'blue').
    """
    colored_hexes = []
    for item in grid:
        if item[0] is None or item[1] is None:
            continue
        (coords, color_code) = item
        if coords is None or len(coords) != 2 or None in coords:
            continue
        q, r = coords
        if color_code == 1:
            color = "red"
        elif color_code == 2:
            color = "blue"
        else:
            continue
        colored_hexes.append((q, r, color))
    return colored_hexes


def pprint(grid, size, figure=True):
    """
    Fonction principale pour afficher la grille hexagonale.
    Génère les hexagones colorés et dessine la grille.
    """
    colored_hexes = generate_colored_hexes(grid)
    img = plot_hex_grid(layout, size, colored_hexes, figure)
    return img


def animate(img_list):
    """
    Crée une animation GIF à partir d'une liste d'images.
    Sauvegarde l'animation dans un fichier 'hexagon_grid.gif'.
    """
    imageio.mimsave("hexagon_grid.gif", img_list, duration=0.5)
