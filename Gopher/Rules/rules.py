from typing import Union, Callable
from .generals_fonctions import (
    grid_tuple_to_grid_list,
    grid_list_to_grid_tuple,
    neighbours,
    cell_value,
    other_player,
    is_grid_empty,
    init_grid,
)
from .utils_plotting import pprint, animate

# Types de base utilisés par l'arbitre
Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]  # case de départ -> case d'arrivée
Action = Union[ActionGopher, ActionDodo]
Player = int  # 1 ou 2
State = list[tuple[Cell, Player]]  # État du jeu pour la boucle de jeu
Score = int
Time = int

Strategy = Callable[[State, Player], ActionGopher]
Environment = tuple[int, Strategy]


def hash_grid(grid: State) -> int:
    """
    Calcule un hash unique pour représenter l'état de la grille.
    """
    hash_value = 0
    for cell, player in grid:
        if cell is not None:
            row, col = cell
            hash_value ^= hash((row, col, player))
    return hash_value


def is_cell_legal(grid: State, player: Player, cell: Cell, size: int) -> bool:
    """
    Checke si une cellule est légale
    """
    if cell_value(grid, cell) == 0:
        adjacent_player = False
        adjacent_other_player = False
        neighbors_values = [
            cell_value(grid, neighbor) for neighbor in neighbours(cell, size)
        ]
        for value in neighbors_values:
            if value == player:
                adjacent_player = True
            if value == other_player(player) and not adjacent_other_player:
                adjacent_other_player = True
            elif value == other_player(player) and adjacent_other_player:
                adjacent_other_player = False
        if adjacent_player:
            return False
        if adjacent_other_player:
            return True
        return is_grid_empty(grid, size)
    else:
        return False


def legals(grid: State, player: Player, size: int) -> list[ActionGopher]:
    """
    Renvoie tous les coups légaux en fonction d'une grid donnée pour un joueur
    """
    legals = []
    for index, cell in enumerate(grid):
        if cell[0] is not None:
            if is_cell_legal(grid, player, cell[0], size):
                legals.append(cell[0])
    return legals


def score(grid: State, player, size: int) -> float:
    """
    Renvoie le score
    """
    if final(grid, 1, size):
        return -1.0
    else:
        return 1.0


def final(grid: State, player: Player, size: int) -> bool:
    """
    Renvoie si la position est finale ou non : plus de coup pour le joueur actuel
    """
    return legals(grid, player, size) == []


def play(grid: State, player: Player, cell: Cell, size: int) -> State:
    """
    Fonction pour jouer un coup
    """
    grid_list = grid_tuple_to_grid_list(grid)
    if is_cell_legal(grid, player, cell, size):
        for i, row in enumerate(grid):
            if row[0] == cell:
                grid_list[i][1] = player
                return grid_list_to_grid_tuple(grid_list)
    else:
        print("Coup non valide")
        return grid


def calculer_prop_plateau(grid: State) -> float:
    """

    Calcule la proportion cellules jouées / cellules non jouées + jouées
    """
    total_cells = non_empty_cells = sum(
        cell != (None, None) for row in grid for cell in row
    )
    counter = 0
    for index, cell in enumerate(grid):
        if cell[0] is not None:
            if cell_value(grid, cell[0]) == 1 or cell_value(grid, cell[0]) == 2:
                counter += 1
    return float(counter) / float(total_cells)


def gopher(
    grid: State,
    strategy_1: Strategy,
    strategy_2: Strategy,
    size: int,
    debug: bool = False,
    gif: bool = False,
    current_player: int = 1,
) -> Score:
    """
    Fonction pour jouer une partie de Gopher
    Possibilité de choisir la taille de plateau, et de partir de ni'importe quelle position pour n'importe quelle position
    """
    fig_list = []
    if final(grid, current_player, size) and (grid != init_grid(size)):
        return score(grid, current_player, size)
    while True:
        if current_player == 1:
            action = strategy_1(grid, 1, size)
        else:
            action = strategy_2(grid, 2, size)

        if action not in legals(grid, current_player, size):
            if debug:
                print("Coup invalide", action)
            continue
        if debug:
            print(
                f"Player {current_player} played at position {action} | Counter{calculer_prop_plateau(grid)}"
            )
        grid = play(grid, current_player, action, size)
        if gif:
            im = pprint(grid, size, False)
            fig_list.append(im)

        current_player = other_player(current_player)
        if final(grid, current_player, size):
            if gif:
                animate(fig_list)
            if debug:
                pprint(grid, size)
                print(f"Vainqueur : PLAYER{other_player(current_player)}")
            return score(grid, current_player, size)
