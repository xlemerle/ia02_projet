# -*- coding: utf-8 -*-
"""
Created on Tue May 21 17:41:59 2024

@author: lemer
"""
from typing import Union
import numpy as np

# Types de base utilisés par l'arbitre
Environment = ...  # Ensemble des données utiles (cache, état de jeu...) pour
# que votre IA puisse jouer (objet, dictionnaire, autre...)
Cell = tuple[int, int]
ActionGopher = Cell
ActionDodo = tuple[Cell, Cell]  # case de départ -> case d'arrivée
Action = Union[ActionGopher, ActionDodo]
Player = int  # 1 ou 2
State = list[tuple[Cell, Player]]  # État du jeu pour la boucle de jeu
Score = int
Time = int


def grid_tuple_to_grid_list(grid: State) -> list[list[list[int, int], Player]]:
    """Transforme un State de tuple en list"""

    grid_list = []
    for row in grid:
        row_list = []
        for element in row:
            # si l'élément est un tuple on convertit en liste
            if isinstance(element, tuple):
                row_list.append(list(element))
            # sinon ajout classique
            else:
                row_list.append(element)
        grid_list.append(row_list)

    return grid_list


def grid_list_to_grid_tuple(grid: list[list[list[int, int], Player]]) -> State:
    """Transforme un State de list en tuple"""
    grid_tuple = []
    for row in grid:
        if isinstance(row[0], list):
            row_tuple = (tuple(row[0]), row[1])
        else:
            row_tuple = tuple(row)
        grid_tuple.append(row_tuple)
    return grid_tuple


def convert_coords(q, r):
    """
    Conversion des coordonnées
    """
    x = q
    y = 0

    # Déterminer la rangée
    if r < 0:
        rangee = -r
    else:
        rangee = r

    # Ajuster y en fonction de la rangée
    if rangee % 2 == 0:
        if r < 0:
            y = rangee
        else:
            y = -rangee
    else:
        if r < 0:
            y = rangee
        else:
            y = -rangee - 1

    return x, y


def pprint_plots(grid: State):
    """affiche la grille comme un plot"""
    print()


def neighbours(cell: Cell, size: int) -> list[Cell]:
    """Fonction qui renvoie tous les voisins d'une cellule"""

    # extraction des coordonnes
    i, j = cell[0], cell[1]

    # extraction des coordonnees voisins
    # coordonnees horaires
    h0 = (i + 1, j + 1)
    h2 = (i + 1, j)
    h4 = (i, j - 1)
    h6 = (i - 1, j - 1)
    h8 = (i - 1, j)
    h10 = (i, j + 1)

    all_neighbours = [h0, h2, h4, h6, h8, h10]
    valid_neighbours = []

    for n in all_neighbours:
        if abs(n[0]) <= size and abs(n[1]) <= size:
            valid_neighbours.append(n)

    return valid_neighbours


def other_player(player: Player) -> Player:
    """
    Renvoie l"autre joueur
    """
    if player == 1:
        return 2
    else:
        return 1


def cell_value(grid: State, cell: Cell) -> int:
    """Renvoie la valeur (0, 1 ou 2) d'une cellule dans une grille"""

    for row in grid:
        if row[0] == cell:
            return row[1]
    return None


def init_grid(size: int):
    """
    Fonction d'initialisation d'une grid
    """
    n = 2 * size + 1
    grid = list_of_lists = [[] for _ in range(n)]
    for i in range(n):
        for j in range(n):
            if abs(i - n) + abs(j - n) <= size + 1 or i + j < size:

                grid[i].append((None, None))
            else:
                grid[i].append(((-(size - j), size - i), 0))
    return [item for sublist in grid for item in sublist]


def is_grid_empty(grid: State, size: int) -> bool:
    """
    Checke si une grille est vide ou non
    """
    return grid == init_grid(size)
