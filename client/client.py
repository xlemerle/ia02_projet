#!/usr/bin/python3

import os
import sys
import ast
import argparse
from typing import Dict, Any
from gndclient import start, Action, Score, Player, State, Time, DODO_STR, GOPHER_STR

Environment = Dict[str, Any]

# import Dodo
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../Dodo/Strategy')))
from dodo_minmax import strategy_minmax

# import Gopher
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../Gopher/Rules')))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../Gopher/Strategies')))

from rules import init_grid, ActionGopher, play
from utils_plotting import pprint
from strategies import strategy_MCTS, strategy_random

jeu_joue = 1

def initialize(
    game: str, state: State, player: Player, hex_size: int, total_time: Time
) -> Environment:
    print("Init")
    grid = init_grid(hex_size - 1)
    best_moves_dict = {}
    print(
        f"{game} playing {player} on a grid of size {hex_size}. Time remaining: {total_time}"
    )
    player = 2
    return {hex_size - 1}


def strategy_brain(
    env: Environment, state: State, player: Player, time_left: Time
) -> tuple[Environment, Action]:
    print("New state ", state)
    print("Time remaining ", time_left)
    print("What's your play ? ", end="")
    s = input()
    print()
    t = ast.literal_eval(s)
    return (env, t)

def strategy_dodo(
    env: Environment, state: State, player: Player, time_left: Time
)  -> tuple[Environment, Action]:
    print("Time remaining ", time_left)
    eval_action = strategy_minmax(state, player, time_left)
    eval, action = eval_action[0], eval_action[1]
    print("Action : ", action)
    return (env, action)

def strategy_MCTS_server(
    env: Environment, state: State, player: Player, time_left: Time
) -> tuple[Environment, Action]:
    taille = list(env)[0]
    action = strategy_MCTS(state, player, taille)
    print("ACTION JOUEE ", action)
    return (env, action)

def final_result(state: State, score: Score, player: Player):
    print(f"Ending: {player} wins with a score of {score}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="ClientTesting", description="Test the IA02 python client"
    )

    parser.add_argument("group_id")
    parser.add_argument("members")
    parser.add_argument("password")
    parser.add_argument("-s", "--server-url", default="http://lagrue.ninja")
    parser.add_argument("-d", "--disable-dodo", action="store_true")
    parser.add_argument("-g", "--disable-gopher", action="store_true")
    args = parser.parse_args()

    strategy = None

    available_games = [DODO_STR, GOPHER_STR]
    if args.disable_dodo:
        available_games.remove(DODO_STR)
    if args.disable_gopher:
        available_games.remove(GOPHER_STR)

    start(
        args.server_url,
        args.group_id,
        args.members,
        args.password,
        available_games,
        initialize,
        strategy_MCTS_server,
        final_result,
        gui=True,
    )
